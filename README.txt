SUMMARY
========

This module can be used to manage the title of blocks page specifically.
So privileged user can add/edit block titles.

INSTALLATION
============

Install as usual,
see https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

Usage
============
The Block Custom Title module provides an Image link near the Block Titles.
Clicking on it, a form is provided where the new title of the block can be
entered and saved.

Authors/maintainers
===================

Original Author:

Clinton Correya
https://drupal.org/user/313751

Co-Maintainers:

Sajini Antony
https://drupal.org/user/2603884
